$(function(){

    /* カウントダウンタイマー ====================================== */
    $('#timer').yycountdown({
        endDateTime : '2016/12/12 00:00:00',
        complete : function(){
            $('#zeroChange').html("<p class=\"changedTxt\">絶賛発売中!</p>")
        }
    });

    /* フォーム入力時にデフォのテキストを消す ========================= */
    $('.inp_mail').focus(function(){
        $(this).attr('value', '').addClass('inp_focus');
    }).blur(function(){
        $(this).attr('value', 'メールアドレスを入力してください')
    })

    /* キービジュアル切り替え ======================================= */
    var kv = $('#chimg'),
        chTime = 3500,
        chNum = 1
        elmNum = 5, //キービジュアル内の要素の数
        flashElmNum = 3, //枠の光の数
        fadeSpeed = 1000;

    // 画像切り替え関数
    var ChangeIMG = function(){
        kv.attr('src', 'images/kv0' + chNum + '.png');
    };

    // 枠切り替え
    var kvFlash = $('.chflash'),
        kvFlashAlw = $('.chflash_always');

    var ChangeFlash = function(chNum){
        if(1 < chNum && chNum < flashElmNum + 2){
            kvFlash.eq(chNum - 2).fadeIn(fadeSpeed);
        } else if(chNum == elmNum){
            setTimeout(function(){
                kvFlash.fadeOut(fadeSpeed);
                kvFlashAlw.fadeTo(fadeSpeed, 0).fadeTo(fadeSpeed, 1);
            }, chTime);
        }
    };

    // settimeoutでループ処理
    var fadeToggleKV = function(){
        // リングの光を先にする為、一周したら遅延
        if(chNum > elmNum){
            chNum = 1;
            setTimeout(function(){
                kv.fadeIn(fadeSpeed);
            }, fadeSpeed * 2);
        } else {
            kv.fadeIn(fadeSpeed);
        }
        ChangeFlash(chNum);
        setTimeout(function(){
            kv.fadeOut(fadeSpeed, ChangeIMG);
            chNum++;
        }, chTime);

        setTimeout(fadeToggleKV, chTime);
    };
    fadeToggleKV();

    /* スクロールボタン ======================================= */

    var scrollBtn = $('#scrollbtn'),
        scrollMoveDuration = 1500,
        scrollBtnPosi = 10,
        moveValue = 1;

    var moveScrollbtn = function(){
        scrollBtn.animate({bottom: scrollBtnPosi + moveValue + "%"}, scrollMoveDuration / 2);
        scrollBtn.animate({bottom: scrollBtnPosi - moveValue + "%"}, scrollMoveDuration / 2);
        setTimeout(moveScrollbtn, scrollMoveDuration);
    };
    moveScrollbtn();

    // スムーズスクロール
    $('a[href^=#]').click(function() {
         var scrollspeed = 500;
         var href= $(this).attr("href");
         var target = $(href == "#" || href == "" ? 'html' : href);
         var position = target.offset().top;
         $('body,html').animate({scrollTop:position}, scrollspeed, 'swing');
         return false;
      });
});


$(function(){
  var $overlay = $('.modal-overlay');
  var $panel = $('.modal');
  var $close = $('.modal-close');
  var $execute = $('.modal-delete');
  var $change = $('.btnList .no');

  $change.click(function(e){
     e.preventDefault();
     $overlay.fadeIn();
     $panel.fadeIn(1);
      setPosition();
      console.log($change);
    $('.changimg').find("img").attr("src", $(this).find("img").attr("src"));
   });

  // 向きを変えたときに画面の中央にウィンドウを表示させます。
  $(window).on('resize', function(){
    setPosition();
  });

  // 半透明のレイヤーをタップしてモーダルを非表示にします。
  $execute.click(function(){
    hideModal();
  });

  // ウィンドウをページ中央に表示させます。
  function setPosition(){
    var panelHeight = $panel.height();
    var windowHeight = $(window).height();
    var adjustPosition = (windowHeight - panelHeight)/2;
    $panel.css("top", adjustPosition);
  }

  // モーダルを非表示にする処理をまとめたもの
  function hideModal() {
    $('.message-list').find('li').removeClass('target');
    $panel.fadeOut();
    $overlay.fadeOut();
  }
});
